# Music Encoding Script

## Description
The program converts lossless audio files to lossy ones for portability. Although not yet tested, the major lossless codecs [flac, alac, wav] should all be accounted for but as of now, they are encoded only to opus. Because not everything is available lossless, you might have some lossy audio files in your source directory so if it detects any, it will copy them over to your encoded directory for you. The program will update your encoded directory as needed meaning that it will only encode/copy source files if it doesn't currently detect them in your encoded directory. Lastly, it will look for any album covers and copy them over in their respective album directories for you.

A few assumptions that the program currently makes (these will go away eventually when I add commandline arguments):

    1. Source music directory is '~/Music/source' 

    2. All encoded music will be stored at './Music/encoded'.

    3. Your source music directory goes as follows
            Source -> Artist -> Album -> Songs
            *Note: All singles should be stored directly under the 'Artist' directory*

## Installation and Execution
1. Clone the repository

`git clone https://gitlab.com/rvndweasel/music-encoding-script.git`

2. Go to where you downloaded the project to and simply execute the python script.

`cd Downloads/music-encoding-script.git/ && python flac.py` (Assuming the repo was downloaded in your *Downloads* folder)

## TODO
 - Fix bug where progressbar doesn't always match how many songs need to be encoded.
 - Add multiproccessing to speed up encoding
 - Add commandline paramters to change various settings such as the source music directory or what codecs to encode to.
