# Imports ------------------------------
import os
import re
import subprocess
from tqdm import trange


# Globals ------------------------------
lossless_ext = ('.flac', '.alac', '.wav')
lossy_ext    = ('.mp3', '.aac', '.opus', '.ogg')
picture_ext  = ('.jpg', '.jpeg', '.png')


# Functions ----------------------------
def clean_dir_path(dir_path):
    # Look for any bad characters such as [$, ', "]
    dir_path_clean = re.sub('(\$)','S',dir_path)
    dir_path_clean = re.sub('(\')','',dir_path_clean)
    dir_path_clean = re.sub('(\")','',dir_path_clean)
    
    # Return clean name
    if dir_path != dir_path_clean:
        return dir_path_clean

    # If name was already clean return original
    return dir_path


def check_filenames():
    if(os.path.isdir(music_dir)):
        # Artist Directories
        for dir_path, dir_names, files in os.walk(music_dir):
            # Number of directories for current directory and the source directory
            num_dir_path = len(dir_path.split('/'))
            num_music_dir = len(music_dir.split('/'))

            # Current directory is the source directory
            if num_dir_path == num_music_dir:
                continue

            # Current directory is the artist directory
            elif num_dir_path == num_music_dir+1:
                artist_path = dir_path

                # Make sure artist directory name doesn't include any bad characters
                artist = clean_dir_path(artist_path).split('/')[-1]

                # dir_list only contains the source directory
                if dir_list[-1] == music_dir.split('/')[-1]:
                    dir_list.append(artist)

                # dir_list last directory is another artist
                elif dir_list[-2] == music_dir.split('/')[-1]:
                    dir_list[-1] = artist
                
                else:
                    continue
                
                # Rename artist directory with the clean name
                dir_list_path = '/'.join(dir_list)
                os.rename(artist_path, dir_list_path)

        # Album Directories
        for dir_path, dir_names, files in os.walk(music_dir):
            # Number of directories for current directory and the source directory
            num_dir_path = len(dir_path.split('/'))
            num_music_dir = len(music_dir.split('/'))

            # Current directory is the source directory
            if num_dir_path == num_music_dir:
                continue

            # Current directory is the album directory
            if num_dir_path == num_music_dir+2:
                album_path = dir_path
                album = clean_dir_path(album_path).split('/')
        
                # dir_list last directory is another artist
                if dir_list[-2] == music_dir.split('/')[-1]:
                    dir_list[-1] = album[-2]
                    dir_list.append(album[-1])

                # dir_list last directory is same artist
                else:
                    dir_list[-1] = album[-1]
                    dir_list[-2] = album[-2]

                # Rename album directory with the clean name
                dir_list_path = '/'.join(dir_list)
                os.rename(album_path, dir_list_path)

        # Song Names
        for dir_path, dir_names, files in os.walk(music_dir):
            for file in files:
                # Full path of audio file
                input_file  = f"{dir_path}/{file}"

                # Rename album directory with the clean name
                song = clean_dir_path(input_file)
                os.rename(input_file, song)


def count_music(music_dir: str):
    # Initialize song counter
    song_count = 0

    # Add to song counter for all flac/mp3 files
    if(os.path.isdir(music_dir)):
        for dir_path, dir_names, files in os.walk(music_dir):
            for file in files:
                if file.endswith(lossless_ext) or file.endswith(lossy_ext):
                    song_count = song_count + 1

    # Return the total song count
    return song_count


def get_output_file(dir_path: str, file: str):
    # Split the directory path
    dir_path_parts = dir_path.split('/')

    # If it is multi-part Album
    if(dir_path_parts[-1].startswith('CD')):

        # Make essential variables for readability
        cd_num = dir_path_parts[-1]
        album  = dir_path_parts[-2]
        artist = dir_path_parts[-3]

        # Make sure output directory path is created
        output_path = f"./encoded/{artist}/{album}/{cd_num}"
        if(not os.path.isdir(output_path)):
            os.makedirs(output_path)

        # Append file to the output path
        output_file_path = f"./encoded/{artist}/{album}/{cd_num}/{file}"

    elif(len(dir_path_parts) == 6):
        # Make essential variables for readability
        artist = dir_path_parts[-1]

        # Make sure output directory path is created
        output_path = f"./encoded/{artist}"
        if(not os.path.isdir(output_path)):
            os.makedirs(output_path)

        # Append file to the output path
        output_file_path = f"./encoded/{artist}/{file}"

    else:
        # Make essential variables for readability
        album  = dir_path_parts[-1]
        artist = dir_path_parts[-2]

        # Make sure output directory path is created
        output_path = f"./encoded/{artist}/{album}"
        if(not os.path.isdir(output_path)):
            os.makedirs(output_path)

        # Append file to the output path
        output_file_path = f"./encoded/{artist}/{album}/{file}"

    # Return the final output path of the file
    return output_file_path


def encode_music(music_dir: str, song_total: int):
    # Setup progress bar
    t = trange(song_total)
    
    if(os.path.isdir(music_dir)):
        # Get all directories and files within specified (input) music directory
        for dir_path, dir_names, files in os.walk(music_dir):
            for file in files:

                # For *.flac files
                if(file.endswith(lossless_ext)):
                    input_file  = f"{dir_path}/{file}"
                    output_file = get_output_file(dir_path, file)

                    # Encode file only if it doesn't exist
                    if not os.path.isfile(f"{os.path.splitext(output_file)[0]}.opus"):
                        command = f"ffmpeg -hide_banner -loglevel error -i '{input_file}' -c:a libopus -b:a 128k '{os.path.splitext(output_file)[0]}.opus'"
                    else:
                        continue

                # For *.mp3 files
                if(file.endswith(lossy_ext)):
                    input_file  = f"{dir_path}/{file}"
                    output_file = get_output_file(dir_path, file)

                    # Encode file only if it doesn't exist
                    if not os.path.isfile(output_file):
                        command = f"cp '{input_file}' '{output_file}'"
                    else:
                        continue

                # For *.jpg files (the covers)
                if(file.endswith(picture_ext)):
                    input_file  = f"{dir_path}/{file}"
                    output_file = get_output_file(dir_path, file)

                    # Encode file only if it doesn't exist
                    if not os.path.isfile(output_file):
                        command = f"cp '{input_file}' '{output_file}'"
                    else:
                        continue

                # Run specified command
                subprocess.run([command], shell=True)

                # Manually update progress
                t.update(1)


# Main Function ------------------------
if __name__ == '__main__':

    # Source music directory
    music_dir = f'{os.path.expanduser("~")}/Music/source'

    # Create output directory if needed
    output_dir = './encoded'
    if(not os.path.isdir(output_dir)):
        print("Output directory not found...creating it.")
        os.mkdir(output_dir)
        print("Succefully created output directory!\n")

    # Global directory list for renaming directories with bad characters
    dir_list = []
    for ele in music_dir.split('/'):
        dir_list.append(ele)

    # Make sure all directories have no troublesome characters
    check_filenames()

    # Get total number of songs to encode
    song_total = count_music(music_dir)
    print(f"Encoding a total of {song_total} songs.\n")

    # Encode the songs
    encode_music(music_dir, song_total)
